console.log('');
console.log('load [script.js]');
console.log('');

// init
const btnAdd = document.getElementById('add');
const elToDo = document.getElementById('todo');
const elCompleted = document.getElementById('completed');
const elToDoContainer = document.getElementsByClassName('todo-container')[0];
const taskTemplate = document.getElementsByClassName('todo-item')[0];

// addEventListener
btnAdd.addEventListener('click', addNewTask);
elToDoContainer.addEventListener('click', onClockContainer);

// functions
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function addNewTask(e) {
  e.preventDefault();

  const elTask = document.getElementsByClassName('header-input')[0];
  if (!elTask.value.length) return;

  let newTask = taskTemplate.cloneNode(true);

  let elLabel = newTask.getElementsByClassName('text-todo')[0];
  elLabel.textContent = elTask.value;


  elToDo.append(newTask);

  const key = 'addTasks';
  let addTasks = JSON.parse(localStorage.getItem(key));

  addTasks = Array.isArray(addTasks) ? addTasks : [];
  if (!addTasks.find(e => e === elLabel)) {
    addTasks.push(elTask.value)
    localStorage.setItem(key, JSON.stringify(addTasks));
  }

  elTask.value = '';
  elLabel.value = '';
}

function doneTask(e) {
  const el = e.target.closest('.todo-item');

  elCompleted.append(el);

  const key = 'doneTasks';
  const elLabel = el.getElementsByClassName('text-todo')[0].textContent;

  let addTasks = JSON.parse(localStorage.getItem(key));

  addTasks = Array.isArray(addTasks) ? addTasks : [];
  if (!addTasks.find(e => e === elLabel)) {
    addTasks.push(elLabel);
    localStorage.setItem(key, JSON.stringify(addTasks));
  }
}

function rmTask(e) {
  const elRm = e.target.closest('.todo-item');
  const elLabel = elRm.getElementsByClassName('text-todo')[0].textContent;

  e.target.closest('.todo-item').remove();

  const key = 'rmTasks';
  let rmTasks = JSON.parse(localStorage.getItem(key));

  rmTasks = Array.isArray(rmTasks) ? rmTasks : [];
  if (!rmTasks.find(e => e === elLabel)) {
    rmTasks.push(elLabel)
    localStorage.setItem(key, JSON.stringify(rmTasks));
  }
}

function onClockContainer(e) {
  if (e.target.className === 'todo-complete') {
    return doneTask(e);
  }

  if (e.target.className === 'todo-remove') {
    return rmTask(e);
  }
}

